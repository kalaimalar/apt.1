import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(
    private firestore: AngularFirestore,
    public fireStorage: AngularFireStorage
  ) { }


  read_residentailDetails() {
    return this.firestore.collection('ResidentDetails').snapshotChanges();
  }


}
