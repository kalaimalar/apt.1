import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { DataService } from '../services/data.service';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Resident } from '../models/resident.model';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  mobileNumber: any;
  code = '';
  loginTrigger: any = true;
  verificationId: any;
  residentsList: any;

  residents$: Observable<any>;
  residentCol: AngularFirestoreCollection<Resident>;
  constructor(
    public afAuth: AngularFireAuth,
    public dataService: DataService,
    private afs: AngularFirestore,

  ) { }

  ngOnInit() {
  }

  onOtpSend() {
    if (this.mobileNumber) {
      console.log('if works');
      this.residentCol = this.afs.collection('ResidentDetails', ref => {
        console.log('ref works');
        return ref.where('primary_mobile', '==', '+' + 91 + this.mobileNumber);
      });

      this.residents$ = this.residentCol.snapshotChanges().pipe(map(actions => {
        return actions.map(a => {
          console.log('action works');
          const data = a.payload.doc.data();
          console.log(data);
          return { data };
        });
      }));

      this.residents$.subscribe(snapshot => {
        if (snapshot.length === 0) {
          this.checksecondnumber();
        } else {
          this.sendotp();
          this.loginTrigger = true;
        }
      });

    } else {
      alert('Add Mobile Number')
    }
  }


  checksecondnumber() {
    this.residentCol = this.afs.collection('residents', ref => {
      return ref.where('secondary_mobile', '==', '+' + 91 + this.mobileNumber);
    });

    this.residents$ = this.residentCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        return { data };
      });
    }))

    this.residents$.subscribe(snapshot => {
      if (snapshot.length === 0) {
        alert('Sorry! Your number is not registered at school.')
      } else {
        this.sendotp();
        this.loginTrigger = false;
      }
    });
  }

  verify() {
    const signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, '' + this.code);
    try {
      firebase.auth().signInWithCredential(signInCredential).then((success) => {
        console.log(success);
      }).catch((err) => {
        console.log(err);
      });
    } catch (e) {
      console.log(e);
    }
  }

  sendotp() {
    this.afAuth
      .signInWithPhoneNumber('+' + 91 + this.mobileNumber, new firebase.auth.RecaptchaVerifier
        (
          're-container', {
          size: 'invisible'
        }))
      .then((creds) => {
        alert(creds);
        this.verificationId = creds.verificationId;
        console.log(this.loginTrigger);
        this.loginTrigger = false;
      })
      .catch(err => alert(err));
  }
}
